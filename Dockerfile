FROM jenkins/jenkins:lts

USER root

RUN apt-get update && apt-get install -y libltdl7 python3-pip python3-venv sshpass && addgroup docker --gid 999 && addgroup jenkins docker
RUN pip install paramiko ansible 
RUN echo StrictHostKeyChecking no >> /etc/ssh/ssh_config

USER jenkins
